-- Based Jeff
-- © NextHendrix 2017
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Commands
import           Control.Monad         (forever)
import qualified Data.ByteString.Char8 as B
import           Ignore
import           Network
import           Network.IRC
import           System.IO
import           URLTitle


myCommandPrefix :: B.ByteString
myCommandPrefix = ","

myServer :: HostName
myServer = "irc.freenode.net"

myChannel :: B.ByteString
myChannel = "#chee-fanclub"

myPort :: PortNumber
myPort = 6667

ignoreList :: [B.ByteString]
ignoreList = ["pinebot", "idalius", "dogbot", "snakedog", "phrikpipe"]

main :: IO ()
main = do
  h <- connectTo myServer $ PortNumber myPort
  -- skip the welcome messages and shit
  initialize h
  forever $ doLoop h

decmsg :: B.ByteString -> Parameter
decmsg = mconcat . tail . msg_params . (\(Just a) -> a) . decode

getmsg :: Message -> Parameter
getmsg = mconcat . tail . msg_params

say :: Handle -> B.ByteString -> IO ()
say h msg = B.hPutStrLn h (encode $ privmsg myChannel msg)

initialize :: Handle -> IO ()
initialize h =
  mapM_ (B.hPutStrLn h) (encode <$> [nickInit, userInit, channelInit])
  where
    nickInit = nick "basedjeff"
    userInit = user "basedjeff" "0" "*" "Based Jeff"
    channelInit = joinChan myChannel


doLoop :: Handle -> IO ()
doLoop h = do
  response <- B.hGetLine h
  let decoded = decode response
  outPut decoded
  where
    outPut (Just m) = do
      B.putStrLn (showMessage m)
      checkMsg h m
    outPut Nothing = do
      B.hPutStrLn stdout "Found Errorful Message"
      return ()

checkMsg :: Handle -> Message -> IO ()
checkMsg h msg
  | not . null $
      [ x
      | x <- ignoreList
      , B.isInfixOf x $ (B.pack . show . msg_prefix) msg
      ] = do
    B.hPutStrLn stdout "User ignored"
    return ()
  | msg_command msg == "PING" = pingu h msg
  | Just url <- isUrlMsg msg = doUrl h url myChannel -- Check for URLs to print
  | otherwise = do
    B.hPutStrLn stdout "Nothing Interesting Found"
    return ()

  --  | lookupMsg message = doLookup handle                                    -- TODO: commands
pingu :: Handle -> Message -> IO ()
pingu h msg = do
  B.hPutStrLn h $ encode . pong . mconcat . msg_params $ msg
  B.hPutStrLn stdout $ encode . pong . mconcat . msg_params $ msg
  B.hPutStrLn stdout "Sent Ping"
