{-# LANGUAGE OverloadedStrings #-}
module URLTitle (isUrlMsg, doUrl, URL) where

import           Control.Monad.Catch
import qualified Data.ByteString.Char8         as B
import           Data.ByteString.Lazy.Internal as BI
import           Data.Monoid
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Network.IRC
import           Network.URI
import           System.IO
import           Text.HTML.TagSoup
import           Text.HTML.TagSoup.Match

type URL = B.ByteString

doUrl :: Handle -> URL -> B.ByteString -> IO ()
doUrl h u chan = do
  B.hPutStrLn h . (encode . privmsg chan) . (<> " (" <> u <> ")") =<<
    getTitle u
  B.hPutStrLn stdout . (encode . privmsg chan) . (<> " (" <> u <> ")") =<<
    getTitle u
  B.hPutStrLn stdout "Sent URL"

getTitle :: URL -> IO B.ByteString
getTitle u = do
  man <- newManager tlsManagerSettings
  let requ = parseRequest_ (B.unpack u)
  let opening = dropWhile (not . tagOpenLit "title" (const True))
  let closing = Prelude.takeWhile (not . tagCloseLit "title")
  req <-
    try $ httpLbs requ man :: IO (Either SomeException (Response BI.ByteString))
  return $
    case req of
      Left _ -> "URL Error:"
      Right a ->
        (B.pack .
         innerText .
         closing . opening . canonicalizeTags . parseTags . responseBody)
          (show <$> a)


isUrlMsg :: Message -> Maybe URL
isUrlMsg msg = lookup True $ zip bools params
  where
    bools = isURI . B.unpack <$> params -- isURI worst function ever
    params = [x | x <- lol, B.last x /= ':']
      where
        lol = (B.words . B.intercalate " " . msg_params) msg

